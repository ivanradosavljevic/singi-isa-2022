package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class ArtikalDTO {
	private Long id;
	private String naziv;
	private String opis;
	private double cena;
	private ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();

	public ArtikalDTO() {
		super();
	}

	public ArtikalDTO(Long id, String naziv, String opis, double cena, ArrayList<KupovinaDTO> kupovine) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.cena = cena;
		this.kupovine = kupovine;
	}
	
	public ArtikalDTO(Long id, String naziv, String opis, double cena) {
		this(id, naziv, opis, cena, new ArrayList<KupovinaDTO>());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public ArrayList<KupovinaDTO> getKupovine() {
		return kupovine;
	}

	public void setKupovine(ArrayList<KupovinaDTO> kupovine) {
		this.kupovine = kupovine;
	}

}
