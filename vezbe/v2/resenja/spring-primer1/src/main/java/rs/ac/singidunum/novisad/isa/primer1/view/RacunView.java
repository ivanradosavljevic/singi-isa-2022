package rs.ac.singidunum.novisad.isa.primer1.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import rs.ac.singidunum.novisad.isa.primer1.model.Racun;
import rs.ac.singidunum.novisad.isa.primer1.service.RacunService;

public class RacunView {
	private RacunService racunService;

	public RacunView() {
		super();
	}

	public RacunView(RacunService racunService) {
		super();
		this.racunService = racunService;
	}

	public RacunService getRacunService() {
		return racunService;
	}

	public void setRacunService(RacunService racunService) {
		this.racunService = racunService;
	}

	protected void glavniMeni() {
		System.out.println("Izaberite opciju:");
		System.out.println("1. Prikaz svih racuna");
		System.out.println("2. Prikaz po broju racuna");
		System.out.println("3. Dodavanje racuna");
		System.out.println("4. Izmena racuna");
		System.out.println("5. Uklanjanje racuna");
		System.out.println("6. Blokiranje racuna");
		System.out.println("0. Izlaz");
	}

	protected String ocitajUlaz() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try {
			return reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	protected void prikazSvih() {
		List<Racun> racuni = racunService.findAll();
		for (Racun r : racuni) {
			System.out.println(r.getBrojRacuna() + "\t" + r.getKorisnik() + "\t" + r.getStanje());
		}
	}

	protected void prikazJednog() {
		System.out.println("Unesite broj racuna: ");
		Racun racun = racunService.findOne(ocitajUlaz());
		if (racun != null) {
			System.out.println(racun.getBrojRacuna() + "\t" + racun.getKorisnik() + "\t" + racun.getStanje());
		} else {
			System.out.println("Racun nije pronadjen!");
		}
	}

	protected void dodajRacun() {
		Racun racun = new Racun();
		System.out.println("Unesite broj racuna: ");
		racun.setBrojRacuna(ocitajUlaz());
		System.out.println("Unesite ime i prezime korisnika: ");
		racun.setKorisnik(ocitajUlaz());
		System.out.println("Unesite stanje: ");
		racun.setStanje(Double.parseDouble(ocitajUlaz()));

		if (racunService.save(racun)) {
			System.out.println("Uspesno zapisan racun!");
		} else {
			System.out.println("Racun nije zapisan!");
		}
	}

	protected void izmeniRacun() {
		Racun racun = new Racun();
		System.out.println("Unesite broj racuna: ");
		racun.setBrojRacuna(ocitajUlaz());
		System.out.println("Unesite ime i prezime korisnika: ");
		racun.setKorisnik(ocitajUlaz());
		System.out.println("Unesite stanje: ");
		racun.setStanje(Double.parseDouble(ocitajUlaz()));

		if (racunService.update(racun)) {
			System.out.println("Uspesno zapisan racun!");
		} else {
			System.out.println("Racun nije zapisan!");
		}
	}

	protected void ukloniRacun() {
		System.out.println("Unesite broj racuna za brisanje: ");
		if (racunService.delete(ocitajUlaz())) {
			System.out.println("Brisanje uspesno!");
		} else {
			System.out.println("Brisanje neuspesno!");
		}
	}

	protected void blokirajRacun() {
		System.out.println("Unesite broj racuna: ");
		String id = ocitajUlaz();

		if (racunService.blokiraj(id)) {
			System.out.println("Racun uspesno blokiran!");
		} else {
			System.out.println("Blokiranje racuna neuspesno!");
		}
	}

	public void show() {
		int izbor = 0;
		do {
			glavniMeni();

			try {
				izbor = Integer.parseInt(ocitajUlaz());
			} catch (Exception e) {
				izbor = -1;
			}

			switch (izbor) {
			case 1:
				prikazSvih();
				break;
			case 2:
				prikazJednog();
				break;
			case 3:
				dodajRacun();
				break;
			case 4:
				izmeniRacun();
				break;
			case 5:
				ukloniRacun();
				break;
			case 6:
				blokirajRacun();
				break;
			}
		} while (izbor != 0);
	}

}
