package rs.ac.singidunum.novisad.isa.primer2.service;

import java.util.List;

import org.springframework.stereotype.Component;

import rs.ac.singidunum.novisad.isa.primer2.model.Racun;
import rs.ac.singidunum.novisad.isa.primer2.repository.RacunRepository;


@Component
public class RacunService {
	private RacunRepository racunRepository;

	public RacunService() {
		super();
	}

	public RacunService(RacunRepository racunRepository) {
		super();
		this.racunRepository = racunRepository;
	}

	public RacunRepository getRacunRepository() {
		return racunRepository;
	}

	public void setRacunRepository(RacunRepository racunRepository) {
		this.racunRepository = racunRepository;
	}

	public List<Racun> findAll() {
		return racunRepository.findAll();
	}

	public Racun findOne(String brojRacuna) {
		return racunRepository.findOne(brojRacuna);
	}

	public boolean save(Racun noviRacun) {
		return racunRepository.save(noviRacun);
	}

	public boolean update(Racun racun) {
		return racunRepository.update(racun);
	}

	public boolean delete(String brojRacuna) {
		return racunRepository.delete(brojRacuna);
	}

	public boolean delete(Racun racun) {
		return racunRepository.delete(racun);
	}

	public List<Racun> findByStanjeBetween(double min, double max) {
		return racunRepository.findByStanjeBetween(min, max);
	}

	public boolean blokiraj(String brojRacuna) {
		Racun r = racunRepository.findOne(brojRacuna);
		if (r == null) {
			return false;
		}

		r.setBlokiran(true);
		r.setStanje(0);
		racunRepository.update(r);
		return true;
	}

	public boolean blokiraj(Racun racun) {
		return this.blokiraj(racun.getBrojRacuna());
	}
}
