package rs.ac.singidunum.novisad.isa.primer2.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import rs.ac.singidunum.novisad.isa.primer2.repository.RacunRepository;
import rs.ac.singidunum.novisad.isa.primer2.service.RacunService;
import rs.ac.singidunum.novisad.isa.primer2.view.RacunView;

@Configuration
public class AppConfiguration {
	@Bean
	public RacunRepository racunRepository() {
		return new RacunRepository();
	}
	
	@Bean
	public RacunService racunService(RacunRepository racunRepository) {
		return new RacunService(racunRepository);
	}
	
	@Bean
	public RacunView racunView(RacunService racunService) {
		return new RacunView(racunService);
	}
}
