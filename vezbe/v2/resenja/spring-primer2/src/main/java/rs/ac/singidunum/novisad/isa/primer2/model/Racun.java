package rs.ac.singidunum.novisad.isa.primer2.model;

public class Racun {
	private String korisnik;
	private String brojRacuna;
	private double stanje;
	private boolean blokiran;

	public Racun() {
		super();
	}

	public Racun(String korisnik, String brojRacuna, double stanje, boolean blokiran) {
		super();
		this.korisnik = korisnik;
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
		this.blokiran = blokiran;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public double getStanje() {
		return stanje;
	}

	public void setStanje(double stanje) {
		this.stanje = stanje;
	}

	public String getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(String korisnik) {
		this.korisnik = korisnik;
	}

	public boolean isBlokiran() {
		return blokiran;
	}

	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}

	@Override
	public String toString() {
		return "Racun [korisnik=" + korisnik + ", brojRacuna=" + brojRacuna + ", stanje=" + getStanje() + "]";
	}
}
