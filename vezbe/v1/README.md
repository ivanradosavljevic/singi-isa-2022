# Vežbe 1

**Cilj vežbi:** Implementirati jednostavnu poslovnu aplikaciju i server i klijent za razmenu jednostavnih podataka.

1. Implementirati poslovnu aplikaicju za upravljanje podacima u banci. Banaka upravlja podacima o korisnicima, njihovim računima, uplatama i isplatama novca.
2. Proširiti aplikaciju tako da joj je moguće pristupati preko mreže, proširenja izvršiti u sledećim koracima:

    1. Implementirati jednostavan echo server.
    2. Implementirati jednostavnog echo klijenta.
    3. Implementirati server za obradu uplata i isplata.
    4. Implementirati klijenta za slanje uplata i isplata.
___
### Dodatne napomene:
* Eclipse EE se može naći na adresi: https://www.eclipse.org/downloads/packages/.
___
