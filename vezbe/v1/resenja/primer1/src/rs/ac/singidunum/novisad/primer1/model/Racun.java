package rs.ac.singidunum.novisad.primer1.model;

public class Racun {
	private Korisnik korisnik;
	private String brojRacuna;
	private double stanje;
	
	public Racun() {
		super();
	}
	
	public Racun(Korisnik korisnik, String brojRacuna, double stanje) {
		super();
		this.setKorisnik(korisnik);
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public double getStanje() {
		return stanje;
	}

	public void setStanje(double stanje) {
		this.stanje = stanje;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		if(this.korisnik != korisnik) {
			this.korisnik = korisnik;
			this.korisnik.dodajRacun(this);
		}

	}
}
