# Vežbe 4

**Cilj vežbi:** Implementirati Spring Boot aplikaciju za upravljanje klijentskim računima.

1. Napistati model za korisnike, račune i transakcije.
2. Namapirati modele na tabele u relacionoj bazi podataka.
3. Napraviti repozitorijume za pristup podacima.
4. Napraviti kontrolere za sve entitete.
___
### Dodatne napomene:
* Dokumentacij za Spring Data JPA: https://docs.spring.io/spring-data/jpa/docs/current/reference/html.
___
