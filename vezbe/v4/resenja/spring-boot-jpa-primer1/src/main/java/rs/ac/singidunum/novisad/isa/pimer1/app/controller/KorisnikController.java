package rs.ac.singidunum.novisad.isa.pimer1.app.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.novisad.isa.pimer1.app.dto.KorisnikDTO;
import rs.ac.singidunum.novisad.isa.pimer1.app.model.Korisnik;
import rs.ac.singidunum.novisad.isa.pimer1.app.service.KorisnikService;

@Controller
@RequestMapping("/api/korisnici")
public class KorisnikController {
	@Autowired
	private KorisnikService korisnikService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<List<KorisnikDTO>> getAllKorisnici() {
		List<KorisnikDTO> korisnici = new ArrayList<KorisnikDTO>();
		for (Korisnik k : this.korisnikService.findAll()) {
			korisnici.add(new KorisnikDTO(k.getId(), k.getIme(), k.getPrezime()));
		}
		return new ResponseEntity<List<KorisnikDTO>>(korisnici, HttpStatus.OK);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<KorisnikDTO> createKorisnik(@RequestBody KorisnikDTO noviKorisnik) {
		Korisnik rezultat = this.korisnikService.save(new Korisnik(null, noviKorisnik.getIme(), noviKorisnik.getPrezime()));
		if(rezultat != null) {
			return new ResponseEntity<KorisnikDTO>(new KorisnikDTO(rezultat.getId(), rezultat.getIme(), rezultat.getPrezime()), HttpStatus.CREATED);
		}
		return new ResponseEntity<KorisnikDTO>(HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<KorisnikDTO> getKorisnik(@PathVariable("id") Long id) {
		Optional<Korisnik> korisnik = this.korisnikService.findById(id);
		if (korisnik.isPresent()) {
			return new ResponseEntity<KorisnikDTO>(
					new KorisnikDTO(korisnik.get().getId(), korisnik.get().getIme(), korisnik.get().getPrezime()),
					HttpStatus.OK);
		}
		return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<KorisnikDTO> updateKorisnik(@PathVariable("id") Long id, @RequestBody KorisnikDTO noviKorisnik) {
		Optional<Korisnik> korisnik = this.korisnikService.findById(id);

		if (korisnik.isPresent()) {
			korisnik.get().setId(id);
			korisnik.get().setIme(noviKorisnik.getIme());
			korisnik.get().setPrezime(noviKorisnik.getPrezime());
			this.korisnikService.save(korisnik.get());
			return new ResponseEntity<KorisnikDTO>(
					new KorisnikDTO(korisnik.get().getId(), korisnik.get().getIme(), korisnik.get().getPrezime()),
					HttpStatus.OK);
		}

		return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<KorisnikDTO> deleteKorisnik(@PathVariable("id") Long id) {
		Optional<Korisnik> korisnik = this.korisnikService.findById(id);
		if (korisnik.isPresent()) {
			this.korisnikService.delete(korisnik.get());
			return new ResponseEntity<KorisnikDTO>(HttpStatus.OK);
		}
		return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
	}
}
