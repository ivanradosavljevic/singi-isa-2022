package rs.ac.singidunum.novisad.isa.pimer1.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;

@Entity
public class Racun {
	@Id
	private String brojRacuna;
	
	@Column(nullable = false)
	private Double stanje;
	
	private boolean blokiran;
	
	@ManyToOne(optional = false)
	private Korisnik korisnik;
	
	@Temporal(TemporalType.DATE)
	private Date datumTransakcije;

	public Racun() {
		super();
	}

	public Racun(Korisnik korisnik, String brojRacuna, double stanje, boolean blokiran, Date datumTransakcije) {
		super();
		this.korisnik = korisnik;
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
		this.blokiran = blokiran;
		this.datumTransakcije = datumTransakcije;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public double getStanje() {
		return stanje;
	}

	public void setStanje(double stanje) {
		this.stanje = stanje;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public boolean isBlokiran() {
		return blokiran;
	}

	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}

//	@Override
//	public String toString() {
//		return "Racun [korisnik=" + korisnik + ", brojRacuna=" + brojRacuna + ", stanje=" + getStanje() + "]";
//	}
}
