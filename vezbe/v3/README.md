# Vežbe 3

**Cilj vežbi:** Implementacija poslovne web aplikacije uz pomoć Spring Boot radnog okvira.

1. Kreirati novi Maven projekat i uključiti zavisnosti za Spring Boot starter web.
2. Prepraviti aplikaciju sa prethodnoh vežbi tako da koristi Spring Boot radni okvir.
3. Dodati kontroler preko kojeg je moguće vršiti CRUD operacije nad artiklima.
___
### Dodatne napomene:
* Dokumentacija Spring Boot radnog okvira: https://docs.spring.io/spring-boot/docs/2.4.3/reference/html/.
___