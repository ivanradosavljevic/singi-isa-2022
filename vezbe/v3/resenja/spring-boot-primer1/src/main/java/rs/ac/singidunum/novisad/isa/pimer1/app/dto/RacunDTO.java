package rs.ac.singidunum.novisad.isa.pimer1.app.dto;

public class RacunDTO {
	private KorisnikDTO korisnik;
	private String brojRacuna;
	private double stanje;
	private boolean blokiran;

	public RacunDTO() {
		super();
	}

	public RacunDTO(KorisnikDTO korisnik, String brojRacuna, double stanje, boolean blokiran) {
		super();
		this.korisnik = korisnik;
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
		this.blokiran = blokiran;
	}

	public KorisnikDTO getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(KorisnikDTO korisnik) {
		this.korisnik = korisnik;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public double getStanje() {
		return stanje;
	}

	public void setStanje(double stanje) {
		this.stanje = stanje;
	}

	public boolean isBlokiran() {
		return blokiran;
	}

	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}
}
