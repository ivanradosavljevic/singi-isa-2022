package rs.ac.singidunum.novisad.isa.pimer1.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.novisad.isa.pimer1.app.dto.KorisnikDTO;
import rs.ac.singidunum.novisad.isa.pimer1.app.dto.RacunDTO;
import rs.ac.singidunum.novisad.isa.pimer1.app.model.Korisnik;
import rs.ac.singidunum.novisad.isa.pimer1.app.model.Racun;
import rs.ac.singidunum.novisad.isa.pimer1.app.service.KorisnikService;
import rs.ac.singidunum.novisad.isa.pimer1.app.service.RacunService;

@Controller
@RequestMapping(path = "/api/racuni")
public class RacunController {
	@Autowired
	private RacunService racunService;

	@Autowired
	private KorisnikService korisnikService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<List<RacunDTO>> getAllRacuni(
			@RequestParam(name = "min", defaultValue = "" + (-Double.MAX_VALUE)) Double min,
			@RequestParam(name = "max", required = false) Double max) {
		if (max == null) {
			max = Double.MAX_VALUE;
		}
		List<Racun> racuni = racunService.findByStanjeBetween(min, max);
		ArrayList<RacunDTO> racuniDTO = new ArrayList<RacunDTO>();
		for (Racun r : racuni) {
			racuniDTO.add(new RacunDTO(
					new KorisnikDTO(r.getKorisnik().getId(), r.getKorisnik().getIme(), r.getKorisnik().getPrezime()),
					r.getBrojRacuna(), r.getStanje(), r.isBlokiran()));
		}

		return new ResponseEntity<List<RacunDTO>>(racuniDTO, HttpStatus.OK);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<RacunDTO> createRacun(@RequestBody RacunDTO racun) {
		Korisnik k = korisnikService.findOne(racun.getKorisnik().getId());
		if (k != null) {
			if (racunService.save(new Racun(k, racun.getBrojRacuna(), racun.getStanje(), racun.isBlokiran()))) {
				return new ResponseEntity<RacunDTO>(racun, HttpStatus.CREATED);
			}
		}

		return new ResponseEntity<RacunDTO>(HttpStatus.NOT_ACCEPTABLE);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.GET)
	public ResponseEntity<RacunDTO> getRacun(@PathVariable("brojRacuna") String brojRacuna) {
		Racun r = racunService.findOne(brojRacuna);
		if (r != null) {
			return new ResponseEntity<RacunDTO>(
					new RacunDTO(new KorisnikDTO(r.getKorisnik().getId(), r.getKorisnik().getIme(),
							r.getKorisnik().getPrezime()), r.getBrojRacuna(), r.getStanje(), r.isBlokiran()),
					HttpStatus.OK);
		}
		return new ResponseEntity<RacunDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.PUT)
	public ResponseEntity<RacunDTO> updateRacun(@PathVariable("brojRacuna") String brojRacuna,
			@RequestBody RacunDTO racun) {
//		racun.setBrojRacuna(brojRacuna);
		Racun r = racunService.findOne(racun.getBrojRacuna());
		Korisnik k = korisnikService.findOne(racun.getKorisnik().getId());
		if (r != null && k != null) {
			r.setBlokiran(racun.isBlokiran());
			r.setKorisnik(k);
			r.setStanje(racun.getStanje());

			if (racunService.update(r)) {
				return new ResponseEntity<RacunDTO>(new RacunDTO(
						new KorisnikDTO(racun.getKorisnik().getId(), racun.getKorisnik().getIme(),
								racun.getKorisnik().getPrezime()),
						racun.getBrojRacuna(), racun.getStanje(), racun.isBlokiran()), HttpStatus.ACCEPTED);
			}
		}
		return new ResponseEntity<RacunDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.DELETE)
	public ResponseEntity<RacunDTO> removeRacun(@PathVariable("brojRacuna") String brojRacuna) {
		if (racunService.delete(brojRacuna)) {
			return new ResponseEntity<RacunDTO>(HttpStatus.OK);
		}
		return new ResponseEntity<RacunDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{brojRacuna}/blokiran", method = RequestMethod.PUT)
	public ResponseEntity<RacunDTO> blokirajRacun(@PathVariable("brojRacuna") String brojRacuna,
			@RequestBody Boolean blokiran) {

		if (blokiran) {
			if (racunService.blokiraj(brojRacuna)) {
				return new ResponseEntity<RacunDTO>(HttpStatus.OK);
			}
			return new ResponseEntity<RacunDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<RacunDTO>(HttpStatus.OK);

	}
}
