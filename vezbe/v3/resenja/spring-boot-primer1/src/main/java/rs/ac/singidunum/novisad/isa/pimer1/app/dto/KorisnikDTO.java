package rs.ac.singidunum.novisad.isa.pimer1.app.dto;

import java.util.ArrayList;

public class KorisnikDTO {
	private Long id;
	private String ime;
	private String prezime;
	private ArrayList<RacunDTO> racuni = new ArrayList<RacunDTO>();
	public KorisnikDTO() {
		super();
	}
	public KorisnikDTO(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public ArrayList<RacunDTO> getRacuni() {
		return racuni;
	}
	public void setRacuni(ArrayList<RacunDTO> racuni) {
		this.racuni = racuni;
	}
}
