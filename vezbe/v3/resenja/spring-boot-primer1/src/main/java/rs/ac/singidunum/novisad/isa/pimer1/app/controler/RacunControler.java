package rs.ac.singidunum.novisad.isa.pimer1.app.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.novisad.isa.pimer1.app.model.Racun;
import rs.ac.singidunum.novisad.isa.pimer1.app.service.RacunService;

@Controller
@RequestMapping(path = "/api/racuni")
public class RacunControler {
	@Autowired
	private RacunService racunService;
	
	@RequestMapping(path="", method = RequestMethod.GET)
	public ResponseEntity<List<Racun>> getAllRacuni() {
		return new ResponseEntity<List<Racun>>(racunService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(path="", method=RequestMethod.POST)
	public ResponseEntity<Racun> createRacun(@RequestBody Racun racun) {
		if(racunService.save(racun)) {
			return new ResponseEntity<Racun>(racun, HttpStatus.CREATED);
		}
		return new ResponseEntity<Racun>(HttpStatus.NOT_ACCEPTABLE);
	}
	
	@RequestMapping(path="/{brojRacuna}", method = RequestMethod.GET)
	public ResponseEntity<Racun> getRacun(@PathVariable("brojRacuna") String brojRacuna) {
		Racun r = racunService.findOne(brojRacuna);
		if(r != null) {
			return new ResponseEntity<Racun>(r, HttpStatus.OK);
		}
		return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path="/{brojRacuna}", method = RequestMethod.PUT)
	public ResponseEntity<Racun> updateRacun(@PathVariable("brojRacuna") String brojRacuna, @RequestBody Racun racun) {
		racun.setBrojRacuna(brojRacuna);
		if(racunService.update(racun)) {
			return new ResponseEntity<Racun>(racun, HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path="/{brojRacuna}", method = RequestMethod.DELETE)
	public ResponseEntity<Racun> removeRacun(@PathVariable("brojRacuna") String brojRacuna) {
		if(racunService.delete(brojRacuna)) {
			return new ResponseEntity<Racun>(HttpStatus.OK);
		}
		return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
	}
}
