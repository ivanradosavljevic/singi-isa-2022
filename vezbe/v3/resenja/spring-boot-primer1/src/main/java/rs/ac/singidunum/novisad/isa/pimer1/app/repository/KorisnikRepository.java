package rs.ac.singidunum.novisad.isa.pimer1.app.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.isa.pimer1.app.model.Korisnik;

@Repository
public class KorisnikRepository {
	private HashMap<Long, Korisnik> korisnici = new HashMap<Long, Korisnik>();
	public KorisnikRepository() {
		super();
		korisnici.put(1l, new Korisnik(1l, "Ime 1", "Prezime 1"));
		korisnici.put(2l, new Korisnik(2l, "Ime 2", "Prezime 2"));
	}
	
	public List<Korisnik> findAll() {
		return new ArrayList<Korisnik>(korisnici.values());
	}

	public Korisnik findOne(Long id) {
		return korisnici.get(id);
	}

	public boolean save(Korisnik noviKorisnik) {
		if (korisnici.containsKey(noviKorisnik.getId())) {
			return false;
		}
		korisnici.put(noviKorisnik.getId(), noviKorisnik);
		return true;
	}

	public boolean update(Korisnik korisnik) {
		if (!korisnici.containsKey(korisnik.getId())) {
			return false;
		}
		korisnici.replace(korisnik.getId(), korisnik);
		return true;
	}

	public boolean delete(Long id) {
		if (!korisnici.containsKey(id)) {
			return false;
		}
		korisnici.remove(id);
		return true;
	}

	public boolean delete(Korisnik korisnik) {
		return delete(korisnik.getId());
	}

}
